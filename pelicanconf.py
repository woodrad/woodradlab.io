AUTHOR = "Mathew Woodyard"
FIRST_NAME = "Mat"
SITENAME = "Mathew Woodyard"
SITEURL = ""

PATH = "public"

STATIC_PATHS = [
    "static/icons/",
    "static/favicon.ico",
    "static/site.webmanifest",
]

EXTRA_PATH_METADATA = {
    "static/favicon.ico": {"path": "favicon.ico"},
    "static/site.webmanifest": {"path": "site.webmanifest"},
}

TIMEZONE = "Etc/UTC"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

# Theme Settings
THEME = "./theme/brutalist"
SITEDESCRIPTION = "Mat Woodyard's site"

MENUITEMS = [
    ("home", "/"),
    ("about", "/pages/about.html"),
    ("tags", "/tags")
]

LOGO = "commupysm.svg"

TWITTER_USERNAME = "ctidelivery"
DEFAULT_USERNAME = "woodrad"

TWITTER = f"https://twitter.com/{TWITTER_USERNAME}"
GITLAB = f"https://gitlab.com/{DEFAULT_USERNAME}"
GITHUB = f"https://github.com/{DEFAULT_USERNAME}"
KEYBASE = f"https://keybase.io/{DEFAULT_USERNAME}/"
MASTODON = f"https://mastodon.social/@{DEFAULT_USERNAME}"
