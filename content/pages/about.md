Title: about

hi, my name is mat.

i'm a data scientist specializing in information security.

my [gitlab](https://gitlab.com/woodrad) and
[github](https://github.com/woodrad) accounts host free software i've written.

you can download my gpg public key
[here](https://keybase.io/woodrad/pgp_keys.asc).
