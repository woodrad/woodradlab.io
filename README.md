[woodrad.gitlab.io](https://woodrad.gitlab.io)
====
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)
[![Twitter Follow](https://img.shields.io/twitter/follow/mat_packets?style=social)](https://twitter.com/mat_packets)
[![Keybase PGP](https://img.shields.io/keybase/pgp/woodrad)](https://keybase.io/woodrad)

My personal web page.

Building
--------

#### Using `podman` (replace `podman` with `docker` if you prefer)
1. [Install](https://podman.io/getting-started/installation.html) podman
2. Build the container using `podman build -t pelican .`
3. Do whatever you want using
   `podman run --rm -it  -v ${PWD}:/app:Z pelican make`. See
   `podman run --rm -it  -v ${PWD}:/app:Z pelican make help` for what's
   available.

#### Using local python
1. Install
   [python 3](http://docs.python-guide.org/en/latest/starting/installation/).
2. Create a virtual environment using `python3 -m venv .venv` and enter it with
   `source .venv/bin/activate`.
3. Install pelican and other required packages with
   `python3 -m pip install -U -r requirements.txt`
4. Do whatever you want using `make`. See `make help` for what's available.


Thanks and credits
------------------
This page is powered by [pelican](https://getpelican.com/) using a theme based
on Matt McManus' [brutalist](https://github.com/mamcmanus/brutalist) theme.


License
-------
code
[agpl v3](https://www.gnu.org/licenses/agpl-3.0.en.html)

words
[creative commons by-nc-sa 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

[![AGPL v3](https://licensebuttons.net/l/GPL/2.0/88x62.png)](https://www.gnu.org/licenses/agpl-3.0.en.html)

[![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
