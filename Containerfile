# Containerfile for running pelican inside a container.
#
# Copyright (C) 2020 Mathew Woodyard
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/agpl-3.0>.
#
# To build:
# $ podman build -t pelican .
#
# To run:
# To display the command line options:
# $ podman run --rm -it pelican make help
# .. will display the command line help
#
# To run `pelican` against code in the current directory using the image
# from GitLab:
# $ podman run --rm -it  -v ${PWD}:/data:Z registry.gitlab.com/woodrad/woodrad.gitlab.io

FROM docker.io/python:3.9.1-buster
WORKDIR /app

RUN addgroup --system pelican \
  && adduser --system --disabled-login --ingroup pelican --no-create-home \
       --gecos "pelican user" --shell /bin/false pelican \
  && chown pelican:pelican /app

COPY requirements.txt /tmp
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt \
  && rm /tmp/requirements.txt

USER pelican

EXPOSE 8000
