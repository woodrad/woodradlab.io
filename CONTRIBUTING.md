# Contributing

All contributions are welcome and will show up publicly on
[woodrad.gitlab.io](https://woodrad.gitlab.io) and
[woodrad.com](https://wwww.woodrad.com).

1. Refer to [the readme](README.md) for setting up your environment.
2. [Fork and merge](https://docs.gitlab.com/ee/workflow/forking_workflow.html)
   the repository.

Always be nice and follow the [code of conduct](CODE_OF_CONDUCT.md).
